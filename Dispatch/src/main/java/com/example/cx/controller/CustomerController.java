package com.example.cx.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.cx.model.Customer;
import com.example.cx.model.Task;
import com.example.cx.service.CustomerService;
import com.example.cx.service.TaskService;

@RestController
@RequestMapping(value = "customers")
public class CustomerController {

	@Autowired
	private TaskService taskService;

	@Autowired
	private CustomerService customerService;

	@GetMapping("/tasks")
	public ResponseEntity<List<Task>> getTasks() {
		List<Task> tasks = taskService.getAll();
		return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);
	}

	@GetMapping("/customers")
	public ResponseEntity<List<Customer>> getCustomers() {
		List<Customer> customers = customerService.getAll();
		return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
	}

	@GetMapping("/dateRange")
	public ResponseEntity<List<Task>> getTasksWithinRange(
			@RequestParam(name = "customerName", required = false) String customername,
			@RequestParam(name = "fromDate", required = false) String fromDateStr,
			@RequestParam(name = "toDate", required = false) String toDateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
		Date fromDate = null;
		Date toDate = null;
		try {
			if (!(fromDateStr.isEmpty()) && !(toDateStr.isEmpty())) {
				fromDate = sdf.parse(fromDateStr);
				toDate = sdf.parse(toDateStr);
			} else {
				fromDate = sdf.parse("2017-01-01");
				toDate = sdf.parse("2019-12-12");
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<Task> tasks = taskService.findByDate(fromDate, toDate);
		return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);
	}
}
