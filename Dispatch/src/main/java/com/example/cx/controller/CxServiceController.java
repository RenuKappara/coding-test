package com.example.cx.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.cx.model.Customer;
import com.example.cx.model.Task;
import com.example.cx.service.CustomerService;
import com.example.cx.service.TaskService;

@RestController
public class CxServiceController {
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/tasks")
	public ResponseEntity<List<Task>> getTasks(){
		List<Task> tasks = taskService.getAll();
		return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);		
	}
	
	@GetMapping("/customers")
	public ResponseEntity<List<Customer>> getCustomers(){
		List<Customer> customers = customerService.getAll();
		return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);		
	}

	@GetMapping("/dateRange")
	public ResponseEntity<List<Task>> getTasksWithinRange(@RequestParam("fDate") String fDate,
			@RequestParam("lDate") String lDate) {
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = simpleDateFormat.parse(fDate);
			d2 = simpleDateFormat.parse(lDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		List<Task> tasks = taskService.findByDate(d1, d2);
		return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);
	}
}
