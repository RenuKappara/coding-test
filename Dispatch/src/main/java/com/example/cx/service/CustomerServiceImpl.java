package com.example.cx.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.cx.model.Customer;
import com.example.cx.repo.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public List<Customer> getAll() {
		List<Customer> customers = (List<Customer>) customerRepository.findAll();
		return customers;
	}

}
