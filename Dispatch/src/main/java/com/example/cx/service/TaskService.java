package com.example.cx.service;

import java.util.Date;
import java.util.List;

import com.example.cx.model.Task;


public interface TaskService {

	public List<Task> getAll();

	public List<Task> findByDate(Date d1, Date d2);
}
