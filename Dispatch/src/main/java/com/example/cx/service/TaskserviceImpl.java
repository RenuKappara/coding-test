package com.example.cx.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.cx.model.Task;
import com.example.cx.repo.TaskRepository;

@Service
public class TaskserviceImpl implements TaskService {

	@Autowired
	private TaskRepository taskRepository;

	@Override
	public List<Task> getAll() {
		List<Task> tasks = (List<Task>) taskRepository.findAll();
		return tasks;
	}

	@Override
	public List<Task> findByDate(Date d1, Date d2) {
		 List<Task> tasks = taskRepository.getTasksInRange(d1, d2);
//		List<Task> tasks = (List<Task>) taskRepository.findAll();
		return tasks;
	}

}
