package com.example.cx.service;

import java.util.List;
import com.example.cx.model.Customer;

public interface CustomerService {
	
	public List<Customer> getAll();

}
