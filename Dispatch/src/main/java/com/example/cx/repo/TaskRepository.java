package com.example.cx.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.cx.model.Task;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {
	
	@Query("Select t from Task t where t.endDate BETWEEN :fDate and :tDate")
	List<Task> getTasksInRange(@Param("fDate") Date fDate, @Param("tDate") Date tDate);

}
