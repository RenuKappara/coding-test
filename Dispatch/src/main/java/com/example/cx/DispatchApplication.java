package com.example.cx;

import java.text.SimpleDateFormat;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.cx.model.Customer;
import com.example.cx.model.Task;
import com.example.cx.repo.CustomerRepository;
import com.example.cx.repo.TaskRepository;

@SpringBootApplication
@EnableAutoConfiguration
public class DispatchApplication {

	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private TaskRepository taskRepository;

	public static void main(String[] args) {
		SpringApplication.run(DispatchApplication.class, args);
	}

	@Bean
	public CommandLineRunner loadCustomerData() {
		return (args) -> {
			customerRepository.save(new Customer("Jack", "8042347354"));
			customerRepository.save(new Customer("David", "8051347356"));
			customerRepository.save(new Customer("Palmer", "8945668001"));
			customerRepository.save(new Customer("Michelle", "7881225674"));
			customerRepository.save(new Customer("Michael", "9091233454"));
		};
	}

	@Bean
	public CommandLineRunner loadTaskData() {
		return (args) -> {
			List<Customer> customers = (List<Customer>) customerRepository.findAll();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
			for (Customer c : customers) {
				Task task = new Task("XYYZ", "Scheduled", "CA", sdf.parse("2018-01-20"), "Y", "N");
				task.setCustomer(c);
				taskRepository.save(task);
			}

			for (Customer c : customers) {
				Task task1 = new Task("ABCD", "Unscheduled", "AZ", sdf.parse("2018-01-23"), "Y", "N");
				task1.setCustomer(c);
				taskRepository.save(task1);
			}

		};
		// return (args) -> {
		// taskRepository.save(new Task(1, "XYYZ", "Scheduled", "CA", new
		// Date(System.currentTimeMillis()),
		// "Y", "N"));
		// taskRepository.save(new Task(2, "yYYZ", "UnScheduled", "MI",
		// "2018-05-03",
		// "N", "N"));
		// taskRepository.save(new Task(3, "zYYZ", "Completed", "NC",
		// "2017-10-11",
		// "Y", "N"));
		// taskRepository.save(new Task(4, "xYYZ", "Completed", "SC",
		// "2017-12-30",
		// "Y", "N"));
		// taskRepository.save(new Task(5, "tYYZ", "Completed", "TN",
		// "2018-03-10",
		// "Y", "N"));
	};
}
