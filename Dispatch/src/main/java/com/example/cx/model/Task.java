package com.example.cx.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Task {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "task_type")
	private String taskType;

	@Column(name = "task_status")
	private String taskStatus;

	@Column(name = "state")
	private String state;

	@Column(name = "end_date")
	private Date endDate;
	
	@Column(name = "notification_message")
	private String notificationMessage;

	@Column(name = "return_message")
	private String returnMessage;

	@ManyToOne(fetch = FetchType.EAGER, cascade={CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name="cus_id")
	private Customer customer;

	public Task() {
	}

	public Task(String taskType, String taskStatus, String state, Date endDate, 
			String notificationMessage, String returnMessage) {
		this.taskType = taskType;
		this.taskStatus = taskStatus;
		this.endDate = endDate;
		this.state = state;
		this.notificationMessage = notificationMessage;
		this.returnMessage = returnMessage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customers) {
		this.customer = customers;
	}


	@Override
	public String toString() {
		return "Task [taskID=" + id + ", taskType=" + taskType + ", taskStatus=" + taskStatus + ", dueDate=" + endDate
				+ ", notificationMessage=" + notificationMessage
				+ ", returnMessage=" + returnMessage + "]";
	}

}
